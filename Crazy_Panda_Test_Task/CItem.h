#pragma once
#include <iostream>
#include <string>
#include <optional>

using namespace std;

enum eRarity
{
	Common = 0,
	Rare,
	Epic
};

enum eType
{
	Melee = 0,
	Range,
	Armour
};

class CItem
{
public:
	CItem(string strName, eType eTItemType, uint32_t nLevel, eRarity eRItemRarity, double dDamage, double dSpeed)
		: m_strName(strName),
		m_eTItemType(eTItemType),
		m_nItemLevel(nLevel),
		m_eRItemRarity(eRItemRarity),
		m_dDamage(dDamage),
		m_dSpeed(dSpeed)
	{};
	CItem(string strName, eType eTItemType, uint32_t nLevel, eRarity eRItemRarity, double dProtection)
		: m_strName(strName),
		m_eTItemType(eTItemType),
		m_nItemLevel(nLevel),
		m_eRItemRarity(eRItemRarity),
		m_dProtection(dProtection)
	{};

	virtual ~CItem() {};

	inline eRarity eGetRarity() { return m_eRItemRarity; };
	inline eType eGetType() { return m_eTItemType; };
	inline uint32_t nGetLevel() { return m_nItemLevel; };

	string strGetRarity();
	string strGetType();

	inline double dGetDamage() { if (m_dDamage.has_value()) return m_dDamage.value(); };
	inline double dGetSpeed() { if (m_dSpeed.has_value()) return m_dSpeed.value(); };
	inline double dGetProtection() { if (m_dProtection.has_value()) return m_dProtection.value(); };

	void vAddDamage(double dBuff) { if (m_dDamage.has_value()) m_dDamage.value() += dBuff; }
	void vAddSpeed(double dBuff) { if (m_dSpeed.has_value()) m_dSpeed.value() += dBuff; }
	void vAddProtection(double dBuff) { if (m_dProtection.has_value())m_dProtection.value() += dBuff; }

	friend ostream& operator<<(ostream& ostOut, CItem& clCurrent)
	{
		ostOut << "Ident: " << clCurrent.m_strName << ", Type: " << clCurrent.strGetType();
		ostOut << ", Level: " << clCurrent.m_nItemLevel << ", Rarity: " << clCurrent.strGetRarity();

		if (clCurrent.m_eTItemType == eType::Armour)
			ostOut << ", Protection: " << clCurrent.m_dProtection.value();
		else
			ostOut << ", Damage: " << clCurrent.m_dDamage.value() << ", Speed :" << clCurrent.m_dSpeed.value();

		return ostOut;
	}

private:
	CItem();
	string m_strName;
	eType m_eTItemType;
	uint32_t m_nItemLevel;
	eRarity m_eRItemRarity;

	optional<double> m_dDamage;
	optional<double> m_dSpeed;
	optional<double> m_dProtection;

};


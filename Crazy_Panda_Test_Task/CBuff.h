#pragma once
#include <tuple>
#include <variant>
#include <unordered_map>
#include "CFilter.h"

using namespace std;

enum eBuffType
{
	DamageBuff = 0,
	SpeedBuff, 
	ProtectionBuff
};


class CBuff
{
public:
	CBuff(string strName, vector<tuple<string, string, variant<eRarity, eType, uint32_t>>> vtplFilters, eBuffType eBTCurrent, double dValue)
		: m_strName(strName),
		m_eBTCurrent(eBTCurrent),
		m_dValue(dValue)
	{
		for (auto it : vtplFilters)
		{
			if (std::get<0>(it) == "level")
				m_vFilters.push_back(CFilter(get<0>(it), get<1>(it), get<uint32_t>(get<2>(it))));
			else
				m_vFilters.push_back(CFilter(get<0>(it), get<2>(it)));
		}
	};

	virtual ~CBuff() {};

	inline eBuffType eGetType() { return m_eBTCurrent; };
	inline double dGetValue() { return m_dValue; };

	inline bool blCheckItem(CItem* ptrCurrentItem) { return m_mpAppliedBuffs.contains(ptrCurrentItem); };
	void addBuff(CItem*);

	bool blCheckApplyAbility(CItem*);

private:
	CBuff();
	string m_strName;
	vector<CFilter> m_vFilters;
	eBuffType m_eBTCurrent;
	double m_dValue;
	unordered_map<CItem*, bool> m_mpAppliedBuffs;
};


#include <iostream>
#include "CBuff.h"

using namespace std;

void vCheckBuffs(vector<CItem>& vpInventory, vector<CBuff>& vpBuffs)
{
	for (auto &itItem : vpInventory)
		for (auto &itBuff : vpBuffs)
			if (!itBuff.blCheckItem(&itItem))
				if (itBuff.blCheckApplyAbility(&itItem))
					itBuff.addBuff(&itItem);
}

int main()
{
	vector<CItem> vInventory = { {"axe_01",  Melee,  3, Common, 12.0, 0.9 },
								{"revolver_01", Range, 5, Common, 42.0, 2.0 },
								{"revolver_02", Range, 3, Rare, 50.0, 2.1 },
								{"machinegun_01", Range, 5, Epic, 83.1, 10.0 },
								{"jacket_01", Armour, 2, Common, 2.0 },
								{"bulletprof_vest_01",  Armour,  5, Rare, 30.0 } };

	vector<CBuff> vBuffs = {{ "eagle_eye", { tuple<string, string, variant<eRarity, eType, uint32_t>>((string)"level", (string)">=", (uint32_t)4), 
							tuple<string, string, variant<eRarity, eType, uint32_t>>((string)"type", "" ,Range)} , DamageBuff, 10},
							{ "donatium_steel", {tuple<string, string, variant<eRarity, eType, uint32_t>>("rarity", "", Rare)} , ProtectionBuff, 15},
							{ "rage_drink", {tuple<string, string, variant<eRarity, eType, uint32_t>>("type", "", Range), 
							tuple<string, string, variant<eRarity, eType, uint32_t>>("type", "", Melee)} , SpeedBuff, 0.4} };

	for (auto it : vInventory)
		cout << it << endl;

	cout << "Buffed" << endl;
	vCheckBuffs(vInventory, vBuffs);

	for (auto it : vInventory)
		cout << it << endl;

	return 0;
}
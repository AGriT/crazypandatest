#include "CFilter.h"

bool CFilter::blCheckFilter(CItem* pclCheckedItem)
{
	if (m_strParam == "level")
	{
		if (m_strSign.has_value())
		{
			if (m_strSign.value() == ">=")
				return pclCheckedItem->nGetLevel() >= get<uint32_t>(m_varValue);
			else if (m_strSign.value() == ">")
				return pclCheckedItem->nGetLevel() > get<uint32_t>(m_varValue);
			else if (m_strSign.value() == "<=")
				return pclCheckedItem->nGetLevel() <= get<uint32_t>(m_varValue);
			else if (m_strSign.value() == "<")
				return pclCheckedItem->nGetLevel() < get<uint32_t>(m_varValue);
			else if (m_strSign.value() == "==")
				return pclCheckedItem->nGetLevel() == get<uint32_t>(m_varValue);
		}
	}
	else if (m_strParam == "type")
	{
		return pclCheckedItem->eGetType() == get<eType>(m_varValue);
	}
	else if (m_strParam == "rarity")
	{
		return pclCheckedItem->eGetRarity() == get<eRarity>(m_varValue);
	}
}
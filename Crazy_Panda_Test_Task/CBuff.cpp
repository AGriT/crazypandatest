#include "CBuff.h"

bool CBuff::blCheckApplyAbility(CItem* pAppliedItem)
{
	for (auto it : m_vFilters)
		if (it.blCheckFilter(pAppliedItem))
			return true;

	return false;
}


void CBuff::addBuff(CItem* ptrAppliedItem)
{
	m_mpAppliedBuffs[ptrAppliedItem] = true;

	switch (m_eBTCurrent)
	{
	case eBuffType::DamageBuff:
		ptrAppliedItem->vAddDamage(m_dValue);
		break;
	case eBuffType::ProtectionBuff:
		ptrAppliedItem->vAddProtection(m_dValue);
		break;
	case eBuffType::SpeedBuff:
		ptrAppliedItem->vAddSpeed(m_dValue);
		break;
	default:
		break;
	}
}
#include "CItem.h"

string CItem::strGetRarity()
{
	switch (m_eRItemRarity)
	{
	case eRarity::Common:
		return "Common";
		break;
	case eRarity::Epic:
		return "Epic";
		break;
	case eRarity::Rare:
		return "Rare";
		break;
	default:
		break;
	}
}

string CItem::strGetType()
{
	switch (m_eTItemType)
	{
	case eType::Armour:
		return "Armour";
		break;
	case eType::Melee:
		return "Melee";
		break;
	case eType::Range:
		return "Range";
		break;
	default:
		break;
	}
}

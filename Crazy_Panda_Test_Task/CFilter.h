#pragma once
#include <iostream>
#include <vector>
#include <optional>
#include <variant>
#include "CItem.h"

using namespace std;


class CFilter
{
public:
	CFilter(string strParam, string strSign, uint32_t nValue)
		:m_strParam(strParam),
		m_strSign(strSign),
		m_varValue(nValue)
	{
	};
	CFilter(string strParam, variant<eRarity, eType, uint32_t> eValue)
		:m_strParam(strParam),
		m_varValue(eValue)
	{
	};
	~CFilter() {};

	bool blCheckFilter(CItem*);

private:
	CFilter();
	string m_strParam;
	optional<string> m_strSign;
	variant<eRarity, eType, uint32_t> m_varValue;
};

